var XmlDSig = (function (){
	
	
	var XmlDsigError = function XMLDsigError(message, code){
		this.name = "XmlDsig Error"
		this.message = message || "Обшибка обработки XML";
	}
	
	XmlDsigError.prototype = Object.create(Error.prototype);
	XmlDsigError.prototype.constructor = XmlDsigError;
	
	var dp = new DOMParser();
	var ds = new XMLSerializer();
	
	/**
	 * Основной объект для управления подписываемым документом. 
	 * На входе XMLDom Document, содержащий подписи или их шаблоны для проверки или подписания соответственно.
	 **/
	var XMLDigitalSignature = function XMLDigitalSignature(xmlData){
		
		if(!(this instanceof XMLDigitalSignature))
			return new XMLDigitalSignature();
		
		if(!!xmlData.documentElement)
			this.xmlData = xmlData;
		else 
			this.xmlData = dp.parseFromString(xmlData,'text/xml');
		
		this.sgnElements = []; // Массив шаблонов подписей для подписания
		this.vrfElements = []; // Массив подписей для проверки
		
		// 2
		var sgnNodeList = this.xmlData.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'Signature');
		
		for(var i=0;i<sgnNodeList.length;i++){
			// 2.1
			var sigEl = new SignatureElement(sgnNodeList[i]);
			if(!sigEl.signatureValue){
				// 2.2
				this.sgnElements.push(sigEl);
				sigEl.initReference();
			} else {
				// Здесь мы сохраним элементы, у которых возможно необходимо будет проверить подпись 
				vrfElements.push(sigEl);
			}
		}
		
		// Пункт 3.
		this.sgnElements.sort(function(a,b){return a.imposedCount - b.imposedCount;});
		if(this.sgnElements[0])
			if(this.sgnElements[0].imposedCount!=0)
				throw new XmlDSigError("There may be a circular reference",3);
	};
	
	/**
	 * Подписание пустых шаблонов подписи.
	 * @todo доделать пункты с 6.4 - 6.8
	 */
	XMLDigitalSignature.prototype.signSignatureTemplates = function(){
		var signer = this.getDefaultSigner();
		// 	Пункт 6. Перебираем массив sgnElements
		for(var i=0;i<this.sgnElements.length;i++){
			var sgnEl = this.sgnElements[i]
			// 6.1 получим Signer соответствующий содержимомму KeyInfo
			var kInfSig = sgnEl.getKeyInfo(signer);
			// 6.2
			var cnAlgo = sgnEl.getCnAlgoName();
			// 6.3
			sgnEl.digestReferences(signer);
			
			// 6.4
			var cnSinfo = sgnEl.canonicalizationAlgo.canonicalise(sgnEl.elementSgnInfo);
			
			/** @TODO 
			// 6.5 пропустим */
			kInfSig.algorithmName = "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411";
			kInfSig.hashAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#gostr3411";
			kInfSig.sigDigester = new Digester("http://www.w3.org/2001/04/xmldsig-more#gostr3411");
			
			// 6.6 рассчет значения ЭЦП
			var sgnVal = kInfSig.createSignature(cnSinfo);
			
			// 6.7 
			sgnEl.setSignatureValue(sgnVal);
		}
	}
	
	/**
	 * @TODO для отладки
	 */
	XMLDigitalSignature.prototype.getTransforms = function(){
		return this.sgnElements[0].ref[0];
	}
	
	
	/*
	 * @TODO Удалить
	 * @param sgnNode
	 * @returns <null,SignatureElement>
	 *
	XMLDigitalSignature.prototype.processSignatureElement = function processSignatureElement(sgnNode){
		
		var sgnInfo = sgnNode.getElementsByTagName('SignedInfo');
		
		if(sgnInfo.length!=1)
			throw new XmlDsigError("Signature element structure wrong",10);
		
		sgnInfo = sgnInfo[0];
		
		var sgnRef = sgnInfo.getElementsByTagName('Reference');
		if(sgnRef.length<1)
			throw new XmlDsigError("SignedInfo element structure wrong",11);
		
		var refs = new Array();
		
		for(var i=0;i<sgnRef.length;i++){
			
			var sgnURI = sgnRef[i].hasAttribute('URI')?sgnRef[i].getAttribute('URI'):"";
			var sdtNode=null;
		
			if(!!sgnURI){
				if(sgnURI.charAt(0)=='#'){
					sgnURI = sgnURI.substr(1,sgnURI.length);
					sdtNode = this.xmlData.getElementById(sgnURI);
					
					if(!!!sdtNode) 
						sdtNode = this.xmlData.querySelector('[*|id='+sgnURI+'],[*|Id='+sgnURI+'],[*|ID='+sgnURI+']');
					var sigEls = sdtNode.getElementsByTagNameNS('http://www.w3.org/2000/09/xmldsig#','Signature');
					
				} else
					sdtNode = this.xmlData.querySelector(sgnURI);
			} else 
				sdtNode = sgnNode.parentNode;
		}
		var nsigEl = new SignatureElement(sgnNode,sdtNode,sgnRef[0]);
		nsigEl.elementSgnInfo = sgnInfo;
		return nsigEl;
	};
	*/
	/*
	 * @TODO Несколько reference
	 * @TODO Устаревший удалить
	 *
	XMLDigitalSignature.prototype.signatureElementsList = function(){
		var sgnNodeList = this.xmlData.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'Signature');
		var elementErrors = [];
		
		for(var e=0;e<sgnNodeList.length;e++){
			try {
				
				var oSignature = new SignatureElement(sgnNodeList[e]); 
				this.sgnElements.push(oSignature);
				oSignature.processReferences();

			} catch(err) {
				elementErrors.push({element: oSignature, error: err});
			}
		}

		
		this.sgnElements.sort(function(a,b){
			return a.imposedCount - b.imposedCount;
		});
		
		if(elementErrors.length)
			throw elementErrors;
		
		if(this.sgnElements.length<1)
			throw new XmlDsigError("There are no signature templates in this document",15);
		
		if(this.sgnElements[0].imposedCount!=0)
			throw new XmlDsigError("There are circular references",16);
		
		return this.sgnElements;
	}
	*/
	
	/**
	 * Signer по умолчанию для элементов, у которых KeyInfo пуст
	 * @returns 
	 */
	XMLDigitalSignature.prototype.getDefaultSigner = function getDefaultSigner(){
		if(!!this.defaultSigner)
			return this.defaultSigner;
		return this.defaultSigner = new Signer();
	};

	/**
	 * Вывести текущее состояние документа в виде текста.
	 * @returns String
	 */
	XMLDigitalSignature.prototype.asText = function asText(){
		return ds.serializeToString(this.xmlData);
	};
	
	/**
	 * Получить объект реализации для каноникализации/трансформации по имени 
	 */
	XMLDigitalSignature.prototype.getTransform = function(name){
		var t = SignatureElement.transforms[name];
		if(!t)
			throw XmlDsigError("Unknown transform get");
		return new t({name:name});
	};
	
	/**
	 * Добавить поддержку новой трансформации путем передачи конструктора, объетка её реализации.
	 */
	XMLDigitalSignature.prototype.addTransformConstructor = function(name, transformObject){
		if((typeof transformObject.prototype.canonicalise) != 'function')
			throw XmlDsigError("Invalid transform object");
		SignatureElement.transforms[name] = transformObject;
	}

	/**
	 * Получить Digester по поддерживаемому алгоритму 
	 */
	XMLDigitalSignature.prototype.getDigester = function(algo){
		return new Digester(algo);
	}
	
	var emptyfunc = function(){
		return "";
	};
	
	var builtIn_DigestAlgorithms = {
			"http://www.w3.org/2000/09/xmldsig#sha1": {Name:"http://www.w3.org/2000/09/xmldsig#sha1"},
			"http://www.w3.org/2000/09/xmldsig#md5": {Name:"http://www.w3.org/2000/09/xmldsig#md5"},
	};
	
	
	/**
	 * Олицетворяет структуру Элемента Signature и соделжит методы для управления ей XMLDSig.
	 */
	var SignatureElement = (function(){
		
		var SignatureElement =	function SignatureElement(elementSignature){
			
			if(!(this instanceof SignatureElement))
				return new SignatureElement(elementSignature);
			this.elementSignature = elementSignature;
			this.ref = [];
			this.imposedCount = 0;
			
			//this.sdtNode = elementSignature4;
			//this.elementSignature4 = elementSignature4; /** @TDO Удалить **/
			//this.elementReference = elementReference; /** @TDO Удалить **/
			//this.elementRef = elementReference;
			//this.signatureRef = [];
			//this.transformationPlan = [];
		};
		
		Object.defineProperty(SignatureElement.prototype,'signatureNS',
				{configurable: false, enumerable: true, value: "http://www.w3.org/2000/09/xmldsig#", writable:false});
		
		Object.defineProperty(SignatureElement.prototype,'elementSgnInfo',
				{configurable: false, enumerable:true
					, get: function(){return this.getSgnInfo();}
				});
				
		Object.defineProperty(SignatureElement.prototype,'signatureValue',
				{configureble: false, enumerable: true
					, get:function(){return this.getSignatureValue();}
					, set:function(newVal){return this.setSignatureValue(newVal);}
				});
		
		Object.defineProperty(SignatureElement.prototype,'cnAlgorithmName',
				{configureble: false, enumerable: true
					, get:function(){return this.getCanAlgoName();}
					, set:function(newVal){return this.setCanAlgoName(newVal);}
				});
		
		/**
		 * Новый метод реализует пукт 2.2 алгоритма.
		 * @param elementReference
		 * @returns
		 */
		SignatureElement.prototype.initReference = function(){
			// 2.2
			var refNodeList = this.elementSgnInfo.getElementsByTagNameNS(this.signatureNS,'Reference');
			var ic =0;
			this.ref = [];
			for(var i=0;i<refNodeList.length;i++){
				var refEl = new ReferenceElement(refNodeList[i]);
				if(!refEl.digestValue){
					// 2.2.2
					this.ref.push(refEl);
					ic += refEl.getImposedCount(); // 2.2.4
				}
			}
			return this.imposedCount = ic;
		}
		
		SignatureElement.prototype.getSignatureValue = function getSignatureValue(){
			var elementSignatureV = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'SignatureValue');
			if(elementSignatureV.length>1)
				throw new XmlDsigError("Signature element structure wrong. Too many 'SignatureValue' elements",21);
			if(elementSignatureV.length==0)
				return null;
			return elementSignatureV[0].textContent;
		}
		
		SignatureElement.prototype.setSignatureValue = function setSignatureValue(sigvaluedata){
			var elementSignatureV = this.elementSignature.getElementsByTagNameNS(this.signatureNS
											, 'SignatureValue');
			if(elementSignatureV.length==0){
				elementSignatureV = this.elementSignature.ownerDocument.createteElementNS('SignatureValue');
				var bef = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'KeyInfo');
				if(bef.length==0){
					bef = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'Object');
					if(bef.length==0)
						elementSignatureV = this.elementSignature.appendChild(elementSignatureV);
					else
						this.elementSignature.insertBefore(elementSignatureV,bef[0]);
				}
				else 
					this.elementSignature.insertBefore(elementSignatureV,bef[0]);
			} else if(elementSignatureV.length!=1)
				throw new new XmlDsigError("Signature element structure wrong. Too many 'SignatureValue' elements",67);
			else
				elementSignatureV = elementSignatureV[0];
			elementSignatureV.textContent = sigvaluedata;
		}
		
		SignatureElement.prototype.getSgnInfo = function(){
			if(!!this.sgnInfo)
				return this.sgnInfo;
			var sgnInfEl = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'SignedInfo');
			
			if(sgnInfEl.length!=1)
				throw new XmlDsigError("Signature element structure wrong: there are no or more than one SignedInfo element",22);
			
			return this.sgnInfo = sgnInfEl[0];
		}
		
		/**
		 * Пункт 6.1. своеобразно реализованный
		 *  
		 * Анализ тега KeyInfo. Инициализация объекта Signer и KeyProvider
		 * 
		 * @param signerProto Прототип объекта Signer
		 * @returns Объект Signer для этого элемента 
		 */
		SignatureElement.prototype.getKeyInfo = function(signerProto){
			var kInfoNode = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'KeyInfo');
			if(kInfoNode.length>1)
				throw new XmlDSigError("More than one KeyInfo tang does not possible",61);
			if(kInfoNode.length<1)
				return signerProto;
			
			kInfoNode = kInfoNode[0];
			
			for(var kp in keyProviders){
				var newKeyProv = keyProviders[kp].prototype.checkTypeOfData(kInfoNode);
				if(!!newKeyProv){
					var signer = new signerProto.constructor();
					signer.setKeyProvider(newKeyProv);
					return signer;
				}
			}
			
			return signerProto;
		}
		
		/**
		 * @DONE 6.2
		 */
		SignatureElement.prototype.getCnAlgoName = function(){
			if(!!this.canonicalizationAlgo)
				return this.canonicalizationAlgo.name;
			var cnAlgoNode = this.elementSgnInfo.getElementsByTagNameNS(this.signatureNS, 'CanonicalizationMethod');
			if(cnAlgoNode.length!=1)
				throw new Exception("SignedInfo structure wrong. Only one CanonicalizationMethod possible", 62);
			var algName = cnAlgoNode[0].getAttribute('Algorithm');
			if(!algName)
				throw new XmlDSigError("CanonicalizationMethod element has no Algorithm attribute",620);
			
			return this.canonicalizationAlgo = XMLDigitalSignature.prototype.getTransform(algName);
		}
		
		/**
		 * @DONE 6.2а
		 */
		SignatureElement.prototype.setCnAlgoName = function(algo){
			this.canonicalizationAlgo = null;
			this.canonicalizationAlgo = XMLDigitalSignature.prototype.getTransform(algo);
			
			var sgnInfo = this.elementSgnInfo;
			var cnAlgoNode = sgInfo.getElementsByTagNameNS(this.signatureNS, 'CanonicalizationMethod');
			if(cnAlgoNode.length>1)
				throw new Exception("SignedInfo structure wrong. Only one CanonicalizationMethod possible", 620);
			if(cnAlgoNode.length = 0){
				cnAlgoNode = sgnInfo.ownerDocument.createElementNS(this.signatureNS,'CanonicalizationMethod');
				if(sgnInfo.childNodes.length>0)
					cnAlgoNode = sgnInfo.insertBefore(cnAlgoNode,sgnInfo.childNodes[0]);
				else
					cnAlgoNode = sgnInfo.appendChild(cnAlgoNode);
			}
			cnAlgoNode.setAttribute('Algorithm',algo);
			return algo;
		}
		
		
		/**
		 * Обработка элементов Reference Пункт 6.3
		 */
		SignatureElement.prototype.digestReferences = function(){
			for(var i=0; i<this.ref.length;i++){
				var digAlgo = new Digester(this.ref[i].digestMethod);
				if( !(transfEls = this.ref[i].getTransformElements() ))
					//this.ref[i].addTransformToDo(this.canonicalizationAlgo); // 6.3.4
					// 6.3.4a Если нет трансформаций - просто трансформируемся в текст
					this.ref[i].addTransformToDo(new Transform({name:"urn://simply-text-transform"}));  
				var transformed = this.ref[i].doTransforms();
				this.ref[i].digestValue = digAlgo.getDigestValue(transformed);
			}
		}
		
		/**
		 * @TODO Кривой 
		 */
		SignatureElement.prototype.switchOff = function(t){
			
			if(t && this.elementSignature.parentNode){
				this.nextSibling = this.elementSignature.nextSibling;
				this.parentNode = this.elementSignature.parentNode
				this.parentNode.removeChild(this.elementSignature);
			} else if(this.parentNode) {
				if(this.nextSibling)
					this.parentNode.insertBefore(this.elementSignature,this.nextSibling);
				else
					this.parentNode.appendChild(this.elementSignature);
			}
		}
		
		/*
		 * @TODO Будет перенесен в digester
		 * @param signer
		 * @param elementReference
		 * @returns
		 */
/*		SignatureElement.prototype.calculateDigestValue = function calculateDigestValue(signer,elementReference){
			var reference = !!elementReference?elementReference.elementRef:this.elementRef;
			var digestTag = reference.getElementsByTagNameNS(this.signatureNS,'DigestMethod');
			if(digestTag.length!=1 || !digestTag[0].hasAttribute('Algorithm'))
				throw new XmlDsigError('DigestMethod tag not found or does not contents valid algorithm referrence',22);
			var digestAlgorithm = digestTag[0].getAttribute('Algorithm');
			var digester = signer.initDigester(digestAlgorithm);
			return digester.getDigestValue(this.executeTransformationPlan(elementReference));
		} */
		
		/*
		 * @TODO Будет перенесен в ElementReference 
		 * @param digestdata
		 * @param elementReference
		 *
		SignatureElement.prototype.setDigestValue = function setDigestValue(digestdata,elementReference){
			var reference = !!elementReference?elementReference.elementRef:this.elementRef;
			var digestTag = reference.getElementsByTagNameNS(this.signatureNS,'DigestValue');
			if(digestTag.length==0){
				digestTag = reference.ownerDocument.createElement('DigestValue');
				digestTag = reference.appendChild(digestTag)
			} else {
				if(digestTag.length!=1)
					throw new XmlDsigError('Reference element structure wrong',14);
				digestTag = digestTag[0];
			}
			
			digestTag.textContent = digestdata;
		}
		*/
		/**
		 * @TODO будет перенесен в Signer
		 */
		SignatureElement.prototype.createSignatureValue = function(signer){
			var sigmetTag = this.elementSignature.getElementsByTagName('SignatureMethod');
			if(sigmetTag.length!=1 || !(sigmetTag[0].getAttribute('Algorithm')))
				throw new XmlDsigError("SignatureMethod tag is wrong or does not contents valid algorithm referrence",16);
			var sigmetAlgorithm = sigmetTag[0].getAttribute('Algorithm');
			signer.setAlgorithmName(sigmetAlgorithm);
			return signer.createSignature(this.executeTransformationPlan({sdtNode: this.elementSgnInfo, canonicalise:true}));
		}
		
		/*
		 * @TODO Будет перенесен в ElementReference
		 * @TODO Удалить
		 *
		SignatureElement.prototype.executeTransformationPlan = function executeTransformationPlan(elementReference){
			
			var elReference = !!elementReference?elementReference:this
			var transformed = elReference.sdtNode.cloneNode(true);
			
			if(!elReference.transfplan){
				this.createTransformationPlan(elReference);
			}
			
			for(var algo in elReference.transfplan){
				if(!transformed.ownerDocument)
					transformed = dp.parseFromString(transformed,'text/xml');
				transformed = elReference.transfplan[algo].canonicalise(transformed);
			}
			
			if(!!transformed.ownerDocument){
				return  ds.serializeToString(transformed);
			}
			
			return transformed;
		};
		*/
		
		/*
		 * @TODO Будет удален
		 * 
		 *
		SignatureElement.prototype.createTransformationPlan = function createTransformationPlan(elementReference){
			
			var canonicalize = this.elementSignature.getElementsByTagNameNS(this.signatureNS,'CanonicalizationMethod');
			if(canonicalize.length!=1 || !canonicalize[0].hasAttribute('Algorithm'))
				throw new XmlDsigError("CanonicalizationMethod tag not found or does not contents valid algorithm referrence",14);
			
			var canonicalize = canonicalize[0].getAttribute('Algorithm');
			var canonicalizeTransform = false;
			var reference = !!elementReference?elementReference:this;
			var transforms = reference.canonicalise?[]:
								reference.elementRef.getElementsByTagNameNS(this.signatureNS,'Transform');
			var transform;
			
			var apply = function(algo){
				if(!!!this.transforms[algo])
					throw new XmlDsigError("Unknown transformation algorithm",14);
				transform = new this.transforms[algo]({sigEl:this, name:algo, reference:reference});
				transform.addToPlan();
			}
			
			
			for(var i=0;i<transforms.length;i++){
				if(!transforms[i].hasAttribute('Algorithm'))
					throw new XmlDsigError("Transform element structure wrong",13);
				var algo = transforms[i].getAttribute('Algorithm');
				canonicalizeTransform |= algo == canonicalize;
				apply.call(this,algo)
			}
			
			if(!canonicalizeTransform && reference!==0)
				apply.call(this,canonicalize);
				
		};
		*/
		
		/**
		 * Олицетворяет элемент Reference
		 */
		ReferenceElement = function(elementReference){
			this.elementReference = elementReference;
			this.deref = null;
			this.sgnRef = [];
			this.transformsToDo = [];
			this.imposedCount =0;
		}
		
		/**
		 * Значение тега DigestValue
		 * @property Base64string
		 */
		Object.defineProperty(ReferenceElement.prototype, 'digestValue',
				{configureble: false, enumerable: true
					, get: function(){return this.getDigestValue();}
					, set: function(newVal){return this.setDigestValue(newVal);}
				});
		
		/**
		 * URI метода вычисления DigestValue
		 * 
		 * @property uri
		 */
		Object.defineProperty(ReferenceElement.prototype, 'digestMethod',
				{configureble: false, enumerable: true
					, get: function(){return this.getDigestMetName();}
					, set: function(newVal){return this.setDigestMetName(newVal)}
		
				});
		
		/**
		 * Возвращает значение digVal
		 * @returns Base64string
		 */
		ReferenceElement.prototype.getDigestValue = function(){
			var digestValue = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'DigestValue');
			if(digestValue.length>1)
				throw new XmlDSigError("Reference element structure wrong: there are no or more than one DigestValue tag",222);
			if(digestValue.length==0)
				return null;
			return digestValue[0].textContent;
		}
		
		/**
		 * @param Base64syting digVal 
		 * Устанавливает значение тега DigestValue
		 */
		ReferenceElement.prototype.setDigestValue = function(digVal){
			var digestValue = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'DigestValue');
			if(digestValue.length>1)
				throw new XmlDSigError("Reference element structure wrong: there are no or more than one DigestValue tag",637);
			if(digestValue.length==0){
				digestValue = this.elementReference.ownerDocument.createElementNS(SignatureElement.prototype.signatureNS,'DigestValue');
				this.elementReference.appendChild(digestValue);
			} else
				digestValue = digestValue[0];
			
			return digestValue.textContent = digVal;
		}
		/**
		 * Получает элемент xml на который ведет Reference
		 * @returns DOMNode
		 */
		ReferenceElement.prototype.dereference = function(){
			if(!!this.deref)
				return this.deref;
			var sgnURI = this.elementReference.getAttribute('URI');
			var sdtNode = null;
			
			if(sgnURI.charAt(0)=='#'){
				sgnURI = sgnURI.substr(1,sgnURI.length-1);
				sdtNode = this.elementReference.ownerDocument.getElementById(sgnURI);
				if(!!!sdtNode) 
					sdtNode = this.elementReference.ownerDocument.querySelector(
							'[*|id='+sgnURI+'],[*|Id='+sgnURI+'],[*|ID='+sgnURI+']');
			} else
				sdtNode = this.elementReference.ownerDocument.querySelector(sgnURI);
			
			return sdtNode;
		}
		
		/**
		 * Пункт 6.3.1
		 */
		ReferenceElement.prototype.getDigestMetName = function(){
			if(!!this.digMetName)
				return this.digMetName;
			var digMetNode = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'DigestMethod');
			if(digMetNode.length!=1)
				throw new Exception("There are only one DigestMethod element possible in Reference element",631);
			digMetNode = digMetNode[0];
			return this.digMetName = digMetNode.getAttribute('Algorithm');
		}
		
		/**
		 * Пункт 6.3.1а
		 */
		ReferenceElement.prototype.setDigestMetName = function(newName){
			var digMetNode = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'DigestMethod');
			if(digMetNode.length>1)
				throw new Exception("There are only one DigestMethod element possible in Reference element",6310);
			if(digMetNode.length==0){
				digMetNode = this.elementReference.ownerDocument.createElementNS(SignatureElement.prototype.signatureNS,'DigestMethod');
				var dvNode = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'DigestValue');
				if(dvNode>0)
					digMetNode = this.elementReference.insertBefore(digMetNode,dvNode[0]);
				else
					digMetNode = this.elementReference.appendChild(digMetNode);
			} else
				digMetNode = digMetNode[0];
			digMetNode.setAttribute('Algorithm',newName);
			return this.digMetName = newName;
		}
		
		
		/**
		 * @TODO <Reference URI=""> Предусмотреть вариант, когда ссылка на содержащий подпись элемент
		 */
		ReferenceElement.prototype.getImposedCount = function(){
			// 2.2.1
			var sdtNode = this.dereference();
			
			// 2.2.3
			var sgnRef = sdtNode.localName=='Signature'?[sdtNode]:sdtNode.getElementsByTagNameNS(this.signatureNS,'Signature');
			this.sgnRef = [];
			
			for(var i=0;i<sgnRef.length;i++){
				// 2.2.3.1
				var sgnEl = new SignatureElement(sgnRef[i]);
				if(!sgnEl.signatureValue)
					this.sgnRef.push(sgnEl);
			}
			
			return this.imposedCount = this.sgnRef.length;
		}
		
		/**
		 * Пункт 6.3.3
		 */
		ReferenceElement.prototype.getTransformElements = function() {
			var transf = this.elementReference.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'Transforms');
			if(transf.length>1)
				throw new XmlDsigError("There are possible only one Transforms element.",633);
			// 6.3.3.2
			if(transf.length>0){
				var lTransfNode = transf[0].getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'Transform');
				if(lTransfNode.length<1)
					throw new XmlDsigError("There are need at least one Transform element if Transforms present",6332);
				for(var i=0;i<lTransfNode.length;i++){
					var algoName = lTransfNode[i].getAttribute('Algorithm'); // 6.3.3.3
					this.addTransformToDo(XMLDigitalSignature.prototype.getTransform(algoName)); // 6.3.3.5
				}
				return lTransfNode;
			}
			
			return null;
		}
		
		/**
		 * @DONE 
		 * 
		 * Пункт 6.3.3.5
		 **/
		ReferenceElement.prototype.addTransformToDo = function(algo){
			this.transformsToDo = algo.prepareToDo(this.transformsToDo)
		}
		
		/**
		 * @DONE Реализован
		 */
		ReferenceElement.prototype.doTransforms = function(){
			if(this.transformsToDo instanceof Array){
				var tr = this.dereference();
				for(var i=0;i<this.transformsToDo.length;i++){
					tr = this.transformsToDo[i].canonicalise(tr);
				}
				return tr;
			}
			return null;
		}
		
		
		
		return SignatureElement
	}());
	
	
	/**
	 * Базовый класс для алгоритма каноникализации либо трансформации
	 */
	var Transform = (function (){
		
		var Transform = function Transform(options) {
			if(!(this instanceof Transform))
				return new Transform(options);
			this.sigEl = options.sigEl;
			this.name = options.name;
			this.reference = options.reference;
		};
		
		/*
		 * @TODO Удалить, заменен методом prepareToDo
		 *
		Transform.prototype.addToPlan = function addToPlan(){
			//var oldplan = !!this.reference?this.reference.transfplan:this.sigEl.transformationPlan;
			if(this.reference.transfplan instanceof Array){
				var newPlan = [];
				for(var k in oldplan){
					if(!(oldplan[k] instanceof this.constructor))
						newPlan.push(oldplan[k]);
				}
				console.log(this.constructor.name)
				newPlan.push(this);
				this.reference.transfplan = newPlan;
			} else
				this.reference.transfplan = [this];
		};
		*/
		
		/**
		 * @DONE Новый
		 */
		Transform.prototype.prepareToDo = function(otherTransforms){
			if(!(otherTransforms instanceof Array))
				otherTransforms = [];
			var newTransforms = [];
			for(var i=0;i<otherTransforms.length;i++ ){
				if(!(otherTransforms[i] instanceof this.constructor))
					newTransforms.push(otherTransforms[i]);
			}
			newTransforms.push(this);
			return newTransforms;
		}
		
/*		Transform.prototype.performElement = function performElement(){
		
		} */
		
		Transform.prototype.canonicalise = function canonicalise(node){
			return ds.serializeToString(node);
		}
		
		return Transform;
		
	}());
	
	/**
	 * Реализация каноникализации c14n-exc
	 */
	var ExclusiveCanonicalisation = (function() {
		
		var ExclusiveCanonicalisation = function ExclusiveCanonicalisation(options) {
			
			if(!(this instanceof ExclusiveCanonicalisation)){
				return new ExclusiveCanonicalisation(options);
			}
			
			options = options || {};
			
			Transform.call(this, options);
			if(options.includeComments===null){
				this.includeComments = this.name.match(/\#WithComments/g);
			} else
				this.includeComments = !!options.includeComments;
			this.inclusiveNamespaces = options.inclusiveNamespaces || [];
			this.escape = escape;
		};
		
		ExclusiveCanonicalisation.prototype = Object.create(
				Transform.prototype, {
					constructor : {
						value : ExclusiveCanonicalisation
					}
				});

		ExclusiveCanonicalisation.prototype.name = function name() {
			return this.name;
		};
		
		ExclusiveCanonicalisation.prototype.canonicalise = function canonicalise(node){
			if((typeof node) == "string")
				node = dp.parseFromString(node,'text/xml');
			
			var res = this._processInner(node);
			
/*			res =  dp.parseFromString(res,'text/xml');
			res.toString = function(){
				return ds.serializeToString(res);
			} */
			return res;
		}
		
/*		ExclusiveCanonicalisation.prototype.canonicalise = function canonicalise(
				node, cb) {
			var self = this;

			// ensure asynchronicity
			setImmediate(function() {
				try {
					var res = self._processInner(node);
				} catch (e) {
					return cb(e);
				}
				return cb(null, res);
			});
		}; */
		
		ExclusiveCanonicalisation.prototype.getIncludeComments = function getIncludeComments() {
			return !!this.includeComments;
		};

		ExclusiveCanonicalisation.prototype.setIncludeComments = function setIncludeComments(
				includeComments) {
			this.includeComments = !!includeComments;
		};

		ExclusiveCanonicalisation.prototype.getInclusiveNamespaces = function getInclusiveNamespaces() {
			return this.inclusiveNamespaces.slice();
		};

		ExclusiveCanonicalisation.prototype.setInclusiveNamespaces = function setInclusiveNamespaces(
				inclusiveNamespaces) {
			this.inclusiveNamespaces = inclusiveNamespaces.slice();

			return this;
		};

		ExclusiveCanonicalisation.prototype.addInclusiveNamespace = function addInclusiveNamespace(
				inclusiveNamespace) {
			this.inclusiveNamespaces.push(inclusiveNamespace);

			return this;
		};

		var _compareAttributes = function _compareAttributes(a, b) {
			if (!a.prefix && b.prefix) {
				return -1;
			}

			if (!b.prefix && a.prefix) {
				return 1;
			}
			
			if(a.prefix && b.prefix && a.namespaceURI!=b.namespaceURI)
				return a.namespaceURI.localeCompare(b.namespaceURI);

			return a.name.localeCompare(b.name);
		};

		var _compareNamespaces = function _compareNamespaces(a, b) {
			var attr1 = a.prefix + a.namespaceURI, attr2 = b.prefix
					+ b.namespaceURI;

			if (attr1 === attr2) {
				return 0;
			}

			return attr1.localeCompare(attr2);
		};

		ExclusiveCanonicalisation.prototype._renderAttributes = function _renderAttributes(
				node) {
			return (node.attributes ? [].slice.call(node.attributes) : [])
					.filter(function(attribute) {
						return attribute.name.indexOf("xmlns") !== 0;
					})
					.sort(_compareAttributes)
					.map(
							function(attribute) {
								return " "
										+ attribute.name
										+ "=\""
										+ escape
												.attributeEntities(attribute.value)
										+ "\"";
							}).join("");
		};

		ExclusiveCanonicalisation.prototype._renderNamespace = function _renderNamespace(
				node, prefixesInScope, defaultNamespace) {
			var res = "", newDefaultNamespace = defaultNamespace, newPrefixesInScope = prefixesInScope
					.slice(), nsListToRender = [];

			var currentNamespace = node.namespaceURI || "";

			if (node.prefix) {
				var foundPrefix = newPrefixesInScope.filter(function(e) {
					return e.prefix === node.prefix;
				}).shift();

				if (foundPrefix
						&& foundPrefix.namespaceURI !== node.namespaceURI) {
					for (var i = 0; i < newPrefixesInScope.length; ++i) {
						if (newPrefixesInScope[i].prefix === node.prefix) {
							newPrefixesInScope.splice(i--, 1);
						}
					}

					foundPrefix = null;
				}

				if (!foundPrefix) {
					nsListToRender.push({
						prefix : node.prefix,
						namespaceURI : node.namespaceURI,
					});

					newPrefixesInScope.push({
						prefix : node.prefix,
						namespaceURI : node.namespaceURI,
					});
				}
			} else if (defaultNamespace !== currentNamespace) {
				newDefaultNamespace = currentNamespace;
				res += " xmlns=\""
						+ escape.attributeEntities(newDefaultNamespace) + "\"";
			}

			if (node.attributes) {
				for (var i = 0; i < node.attributes.length; i++) {
					var attr = node.attributes[i], foundPrefix = null;

					if (attr.prefix && attr.prefix !== "xmlns") {
						foundPrefix = newPrefixesInScope.filter(function(e) {
							return e.prefix === attr.prefix;
						}).shift();

						if (foundPrefix
								&& foundPrefix.namespaceURI !== attr.namespaceURI) {
							for (var i = 0; i < newPrefixesInScope.length; ++i) {
								if (newPrefixesInScope[i].prefix === attr.prefix) {
									newPrefixesInScope.splice(i--, 1);
								}
							}

							foundPrefix = null;
						}
					}

					if (attr.prefix && !foundPrefix && attr.prefix !== "xmlns" && attr.prefix !== 'xml') {
						nsListToRender.push({
							prefix : attr.prefix,
							namespaceURI : attr.namespaceURI,
						});

						newPrefixesInScope.push({
							prefix : attr.prefix,
							namespaceURI : attr.namespaceURI,
						});
					} else if (attr.prefix
							&& attr.prefix === "xmlns"
							&& this.inclusiveNamespaces.indexOf(attr.localName) !== -1) {
						nsListToRender.push({
							prefix : attr.localName,
							namespaceURI : attr.nodeValue,
						});
					}
				}
			}

			nsListToRender.sort(_compareNamespaces);

			for (var i = 0; i < nsListToRender.length; ++i) {
				res += " xmlns:"
						+ nsListToRender[i].prefix
						+ "=\""
						+ escape
								.attributeEntities(nsListToRender[i].namespaceURI)
						+ "\"";
			}

			return {
				rendered : res,
				newPrefix: node.prefix,
				newDefaultNamespace : newDefaultNamespace,
				newPrefixesInScope : newPrefixesInScope,
			};
		};

		ExclusiveCanonicalisation.prototype._renderComment = function _renderComment(
				node) {
			var isOutsideDocument = (node.ownerDocument === node.parentNode), isBeforeDocument = null, isAfterDocument = null;

			if (isOutsideDocument) {
				var nextNode = node, previousNode = node;

				while (nextNode !== null) {
					if (nextNode === node.ownerDocument.documentElement) {
						isBeforeDocument = true;
						break;
					}

					nextNode = nextNode.nextSibling;
				}

				while (previousNode !== null) {
					if (previousNode === node.ownerDocument.documentElement) {
						isAfterDocument = true;
						break;
					}

					previousNode = previousNode.previousSibling;
				}
			}

			return (isAfterDocument ? "\n" : "") + "<!--"
					+ escape.textEntities(node.data) + "-->"
					+ (isBeforeDocument ? "\n" : "");
		};

		ExclusiveCanonicalisation.prototype._renderProcessingInstruction = function _renderProcessingInstruction(
				node) {
			if (node.tagName === "xml") {
				return "";
			}

			var isOutsideDocument = (node.ownerDocument === node.parentNode), isBeforeDocument = null, isAfterDocument = null;

			if (isOutsideDocument) {
				var nextNode = node, previousNode = node;

				while (nextNode !== null) {
					if (nextNode === node.ownerDocument.documentElement) {
						isBeforeDocument = true;
						break;
					}

					nextNode = nextNode.nextSibling;
				}

				while (previousNode !== null) {
					if (previousNode === node.ownerDocument.documentElement) {
						isAfterDocument = true;
						break;
					}

					previousNode = previousNode.previousSibling;
				}
			}

			return (isAfterDocument ? "\n" : "") + "<?" + node.tagName
					+ (node.data ? " " + escape.textEntities(node.data) : "")
					+ "?>" + (isBeforeDocument ? "\n" : "");
		};
		
		ExclusiveCanonicalisation.prototype._renderText = function(node){
			return (node.ownerDocument === node.parentNode) ? escape
					.textEntities(node.data.trim()) : escape
					.textEntities(node.data);
		}

		ExclusiveCanonicalisation.prototype._processInner = function _processInner(
				node, prefixesInScope, defaultNamespace) {
			defaultNamespace = defaultNamespace || "";
			prefixesInScope = prefixesInScope || [];
			
			if (node.nodeType === 3) {
				return this._renderText(node);
			}

			if (node.nodeType === 7) {
				return this._renderProcessingInstruction(node);
			}

			if (node.nodeType === 8) {
				return this.includeComments ? this._renderComment(node) : "";
			}

			if (node.nodeType === 10) {
				return "";
			}

			var ns = this._renderNamespace(node, prefixesInScope,
					defaultNamespace);

			var self = this;

			return [
					node.localName ? "<" + (ns.newPrefix?ns.newPrefix+":":"") + node.localName 
							+ ns.rendered
							+ this._renderAttributes(node, ns.newPrefixesInScope) + ">" : "",
					[].slice.call(node.childNodes).map(
							function(child) {
								return self._processInner(child,
										ns.newPrefixesInScope,
										ns.newDefaultNamespace);
							}).join(""),
						node.localName ? "</" + (ns.newPrefix?ns.newPrefix+":":"") + node.localName + ">" : "", ].join("");
		};
		
		return ExclusiveCanonicalisation;
	}());
	
	
	
	
	var EnvelopedSignatureCanonicalisation = (function (){
		
		var EnvelopedSignatureCanonicalisation = function EnvelopedSignatureCanonicalisation(options){
			if(!(this instanceof Transform))
				return new EnvelopedSignatureCanonicalisation(options);
			Transform.call(this,options);
		};
		
		
		EnvelopedSignatureCanonicalisation.prototype =  Object.create(Transform.prototype, {
					constructor : { value : EnvelopedSignatureCanonicalisation}
				});
		
		EnvelopedSignatureCanonicalisation.prototype.canonicalise = function canonicalise(node){
			
			var signatureTag = node.getElementsByTagNameNS('http://www.w3.org/2000/09/xmldsig#','Signature');
			for(var k=0;k<signatureTag.length;k++){
				signatureTag[k].parentNode.removeChild(signatureTag[k]);
			}
			return node;
		};
		
		return EnvelopedSignatureCanonicalisation
	}());
	
	
	builtIn_SignatureMethods = {};
	
	var builtIn_Transforms = {
			// c14n classic
			"http://www.w3.org/TR/xml-c14n#": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2001/REC-xml-c14n-20010315": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2001/PR-xml-c14n-20010119": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/CR-xml-c14n-20001212": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/CR-xml-c14n-20001026": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20001011": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20000907": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20000710": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20000613": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20000601": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2000/WD-xml-c14n-20000119": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/1999/WD-xml-c14n-19991115": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/1999/WD-xml-c14n-19991109": ExclusiveCanonicalisation,
			"http://www.w3.org/1999/07/WD-xml-c14n-19990729": ExclusiveCanonicalisation,
			
			// exc-c14n
			"http://www.w3.org/TR/xml-exc-c14n": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2002/REC-xml-exc-c14n-20020718": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2002/PR-xml-exc-c14n-20020524": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2002/CR-xml-exc-c14n-20020212": ExclusiveCanonicalisation,
			"http://www.w3.org/TR/2001/WD-xml-exc-c14n-20011120": ExclusiveCanonicalisation,
			
			// exc-c14n CADES
			"http://www.w3.org/2001/10/xml-exc-c14n#": ExclusiveCanonicalisation,
			"http://www.w3.org/2001/10/xml-exc-c14n#WithComments": ExclusiveCanonicalisation,
			
			"http://www.w3.org/2000/09/xmldsig#enveloped-signature": EnvelopedSignatureCanonicalisation,
		};

	
	
	var escape = (function() {
		var escape = {};
		
		var entities = escape.entities = {
				  "&":  "&amp;",
				  "\"": "&quot;",
				  "<":  "&lt;",
				  ">":  "&gt;",
				  "\t": "&#x9;",
				  "\n": "&#xA;",
				  "\r": "&#xD;",
				};

		var attributeEntities = escape.attributeEntities = function escapeAttributeEntities(string) {
				  return string.replace(/([\&<"\t\n\r])/g, function(character) {
				    return entities[character];
				  });
		};

		var textEntities = escape.textEntities = function escapeTextEntities(string) {
			return string.replace(/([\&<>\r])/g, function(character) {
				return entities[character];
			});
		};	
		
		return escape;
	}());
	
	
	/**
	 * @TODO заменить на KeyProvider
	 */
	var Certificate = function(){
		
	};
	
	Certificate.prototype.getCertString = emptyfunc;
	Certificate.prototype.getCertFromDate = emptyfunc;
	Certificate.prototype.getCertTillDate = emptyfunc;
	Certificate.prototype.getPubKeyAlgorithm = emptyfunc;
	Certificate.prototype.getCertName = emptyfunc;
	Certificate.prototype.getIssuer = emptyfunc;
	Certificate.prototype.thumbprint = emptyfunc;
	
	
	
	/**
	 * Олицетворяет реализацию алгоритам рассчета хэш-функции. 
	 * Ему принадлежит метод для рассчета DigestValue у тега Reference.  
	 */
	var Digester =  function Digester(algo){
		if(!!algo){
			if(!!algo.name){
				this.constuctor.registerAlgorithm(algo);
				algo = algo.name;
			}
			this.setAlgoritmName(algo);
		}
	};
	
	Digester.prototype.digestAlgorithms = builtIn_DigestAlgorithms;
	
	Digester.prototype.setAlgoritmName = function(name){
		if(!!this.digestAlgorithms[name])
			this.applyAlgorithmOptions(this.digestAlgorithms[name]);
		else 
			throw new XmlDsigError("Unknown digest algorithm",21);
	}
	
	Digester.registerAlgorithm = function(algoritm){
		if(!!algorithm.name)
			this.prototype.digestAlgorithms[algorithm.name] = algorithm;
	};
	
	Digester.prototype.applyAlgorithmOptions = function applyAlgorithmOptions(options){
		if(!!options)
			for(var op in options)
				this[op] = options[op];
	};
	
	
	Digester.prototype.hashValue = function hashValue(data){
		if(!!this.name)
			throw new XmlDsigError('Digester not implemented',33);
	};
	
	/**
	 * Этот метод реализуется при адаптации к криптопровайдеру
	 */
	Digester.prototype.getDigestValue = function (){
		
	};
	
	/**
	 * Объект Signer олицетворяет лицо, установившее ЭЦП и алгоритм подписи который при этом использовался.
	 * На нем реализуются методы для рассчета SignatureValue. Является фабрикой для объекта Digester.
	 */
	var Signer = function Signer(){
		
	};
	
	
	Signer.prototype.signatureAlgorithms = {};

	/**
	 * @TODO Заменить после реализации механизма KeyProvider на getKeyProvider()->findSystemCertificate
	 */
	Signer.prototype.useCertificate = function useCertificate(certdata){
		this.certificate = certdata;
	};
	
	/**
	 * @TODO Заменить после реализации механизма KeyProvider на getKeyProvider()->getSystemCertificate
	 */
	Signer.prototype.getCertificate = function getCertificare(certInfo){
		var Certificate = this.getCertificateConstructor();
		if(this.certificate instanceof Certificate)
			return this.certificate;
		return new Certificate();
	};
	
	/**
	 * @TODO Костыль удалить после реализации KeyProvider
	 */
	Signer.prototype.getCertificateConstructor = function getCertificateConstructor(){
		return Certificate;
	};
	
	/*
	 * @TODO Удалить заменить на new Digester();
	 *
	Signer.prototype.initDigester = function initDigester(algo){
		return new Digester();
	};

	Signer.prototype.setAlgoritmName = function (algo) {
		throw new XmlDsigError("Unsupported signature algorithm");
	};
	
	Signer.prototype.createSignature = function(data){
		throw new XmlDsigError("Signature algoritm is undefined");
	};
	*/
	
	/**
	 * Получает конструктор keyProvider
	 * 
	 * @params type тип KeyProvider
	 * @returns KeyProvider
	 */
	Signer.prototype.getKeyProvider = function(type){
		if(!type)
			return this.keyProvider;
		
		if(!!keyProviders[type]){
			var KeyProvider = keyProviders[type];
			if(!(this.keyProvider instanceof KeyProvider))
				return this.keyProvider = new KeyProvider();
			else return this.keyProvider;
		} else
			throw new XmlDSigError("Unknown key provider",4);
			
		 
	}
	
	/**
	 * @TODO реализовать
	 */
	Signer.prototype.setKeyProvider = function(keyprov){
		
	}
	
	/**
	 * Должен быть реализован в криптоадаптере
	 * @returns Base64string
	 */
	Signer.prototype.createSignature = function(data){
		throw new XmlDsigError("There are no cryptoadapter implementation for createSignature in this Signer");
	}
	
	/**
	 * Должен быть реализовае в криптоадаптере
	 */
	Signer.prototype.verifySignature = function(data,signature){
		throw new XmlDsigError("There are no cryptoadapter implementation for verifySignature in this Signer");
	}
	
	/**
	 * @TODO
	 * Олицетворяет методику работы с открытыми ключами. Несет в себе две функции
	 * 1. Получение сертификатов из Storage по определенному полями этого объекта критерию
	 * 2. Работа с XML
	 * 
	 * 2.1. Получение из тега KeyInfo, находящегося в шаблоне подписи сведений о сертификате 
	 * и инициализация собственных полей для последующего поиска сертификата 
	 * 2.2. Формирование содержимого KeyInfo для готовой подписи в определенном наследником формате.   
	 */
	var KeyProvider = function(){
		
	}
	
	
	KeyProvider.prototype.checkTypeOfData = function(keyInfo){
		
	}
	
	var X509KeyProvider = function(){
		this.name = 'X509';
	}
	
	X509KeyProvider.prototype = new KeyProvider;
	X509KeyProvider.parent = KeyProvider;
	X509KeyProvider.prototype.constructor = X509KeyProvider;

	X509KeyProvider.prototype.checkTypeOfData = function(keyInfo){
		var x509data = keyInfo.getElementsByTagNameNS(SignatureElement.prototype.signatureNS,'X509Data');
		if(x509data.length<1)
			return null; 
		if(x509data.length>1)
			throw new XmlDSigError("There are only one X509Data tag possible.",6101);
		
		var kp = new this.constructor();
		return kp.initData(x509data[0]);
		
	}
	
	X509KeyProvider.prototype.initData = function(x509data){
		for(var i=0; i<x509data.childNodes;i++){
			var cn = x509data.childNodes[i];
			if(cn.nodeType==1)
				switch(cn.localName){
				case 'X509IssuerSubjectName': this.subjectName = cn.textContent; break;
				case 'X509IssuerSerial': this.issuerSerial = cn.textContent; break;
				case 'X509Certificate': this.certificateB64 = cn.textContent; break;
				case 'X509CRL': this.crlb64 = cn.textContent; break;
				}
		}
		return this;
	}
	
	
	var keyProviders = {
			'X509': X509KeyProvider 
	}
	
	SignatureElement.transforms = builtIn_Transforms;
	return XMLDigitalSignature;
	
	
}());