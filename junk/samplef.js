	function xmlTest(){
		var dsig = new XmlDSig(Tools.string.value);
		var sigs = dsig.getSignatureElements();
		var sigel = dsig.processSignatureElement(sigs[0])
		sigel.createTransformationPlan();
		var digester = sigel.getDefaultDigester();
		
		digester.hashData = cadesplugin.CreateObject('CAdESCOM.HashedData');
		digester.applyAlgorithmOptions = function (options){
			this.constructor.prototype.applyAlgorithmOptions.call(this,options);
			this.hashData.Algorithm = this.Algorithm;
			this.hashData.DataEncoding = 1;
		}
		
		digester.hashValue = function(data){
			this.hashData.Hash(Tools.base64_encode(data));
			return Tools.base64_encode(Tools.hexToBinaryString(this.hashData.Value));
		}
		
		Tools.hashes.value = sigel.getSignatureDigest();
		
	}
	