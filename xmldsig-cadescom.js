	/** 
	 * Реализует поддержку CADeSCOM для XmlDSig 
	 * 
	 ***/
(function(){
	
	/**
	 * Вспомогательные элементы
	 */
	var Tools = XmlDSig.Tools = window.Tools = (function () {
		
		var Tools = {};
		
		Tools.binaryStringToHex = function binaryStringToHex(binstr){
			var r = '';
			for(var i=0;i<binstr.length;i++){
				var number = Number(binstr.charCodeAt(i));
				var t = (number<0?(0xFFFFFFFF + number + 1):number).toString(16).toUpperCase();
				if(t.length!=2)
					t = '0'+t;
				r += t;
			}	
			return r;
		};
	
		
		Tools.hexToBinaryString = function hexToBinaryString(hexStr, inv){
			var r= '';
			for(var i=0;i<hexStr.length;){
				var c = String.fromCharCode(Number.parseInt("0x"+hexStr.slice(i++,++i)));  
				r = inv?(c + r): (r + c); 
			}
			return r;
		};
		
		
		Tools.b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="; 
		
		Tools.base64_decode = function base64_decode(str){
			// Decodes data encoded with MIME base64
			// 
			// +   original by: Tyler Akins (http://rumkin.com)
			
			var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

			do {  // unpack four hexets into three octets using index points in b64
				h1 = Tools.b64.indexOf(str.charAt(i++));
				h2 = Tools.b64.indexOf(str.charAt(i++));
				h3 = Tools.b64.indexOf(str.charAt(i++));
				h4 = Tools.b64.indexOf(str.charAt(i++));

				bits = h1<<18 | h2<<12 | h3<<6 | h4;

				o1 = bits>>16 & 0xff;
				o2 = bits>>8 & 0xff;
				o3 = bits & 0xff;

				if (h3 == 64)	  enc += String.fromCharCode(o1);
				else if (h4 == 64) enc += String.fromCharCode(o1, o2);
				else			   enc += String.fromCharCode(o1, o2, o3);
			} while(i < str.length);

			return enc;
		}
		
		
		Tools.stringSlice = function(str,ln){
			var ch = str.split("");
			if(!ln) ln=64;
			for(var i=0;i<ch.length-ln;i++){
				ch.splice(i+=ln,0,"\n");
			}
			return ch.join("");
		}
		
		Tools.base64_encode = function base64_encode(str){
			// Encodes data with MIME base64
			// 
			// +   original by: Tyler Akins (http://rumkin.com)
			// +   improved by: Bayron Guevara

			//var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
			var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

			do { // pack three octets into four hexets
				o1 = str.charCodeAt(i++);
				o2 = str.charCodeAt(i++);
				o3 = str.charCodeAt(i++);

				bits = o1<<16 | o2<<8 | o3;

				h1 = bits>>18 & 0x3f;
				h2 = bits>>12 & 0x3f;
				h3 = bits>>6 & 0x3f;
				h4 = bits & 0x3f;

				// use hexets to index into b64, and append result to encoded string
				enc += Tools.b64.charAt(h1) + Tools.b64.charAt(h2) + Tools.b64.charAt(h3) + Tools.b64.charAt(h4);
			} while (i < str.length);

			switch( str.length % 3 ){
				case 1:
					enc = enc.slice(0, -2) + '==';
				break;
				case 2:
					enc = enc.slice(0, -1) + '=';
				break;
			}

			return enc;
		}
		
		Tools.utf8ToBinStr = function(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";

            for (var n = 0; n < string.length; n++) {
        
                var c = string.charCodeAt(n);

                if (c < 128) {
                        utftext += String.fromCharCode(c);
                }
                else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
                else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }
            
            return utftext;
	    }
		
		Tools.binStrToUtf8 = function(utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;

            while (i < utftext.length) {
        
                c = utftext.charCodeAt(i);

                if (c < 128) {
                        string += String.fromCharCode(c);
                    i++;
                }
                else if ((c > 191) && (c < 224)) {
                        c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                }
                else {
                        c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }

            }

            return string;
        }
		return Tools;
	}());
	
	
	var SignerPrototype = XmlDSig.prototype.getDefaultSigner.call({});
	
	var CadesSigner;
	var CadesDigester;
	
	/**
	 * @todo Добавить возможность получения системно сертификата от keyprovider 
	 * @returns
	 */
	XmlDSig.prototype.getDefaultSigner = function getDefaultSigner(){
		
		if(!(CadesSigner instanceof Function)){
			
			CadesSigner = function CadesSigner(){
				if(!(this instanceof CadesSigner))
					return new CadesSigner();
				this.rawSignature = cadesplugin.CreateObject('CAdESCOM.RawSignature');
				SignerPrototype.call(this);
			};
			
			CadesSigner.prototype = SignerPrototype;
			SignerPrototype = SignerPrototype.constructor;
			CadesSigner.prototype.constructor = CadesSigner;
			
			var signAlg;
			for(signAlg in builtIn_SignatureMethods){
				CadesSigner.prototype.signatureAlgorithms[signAlg] 
					= builtIn_SignatureMethods[signAlg]
				CadesSigner.prototype.signatureAlgorithms[signAlg].SignerConstructor = CadesSigner;
			}
		}
		
		if(this.defaultSigner instanceof CadesSigner)
			return this.defaultSigner;
		return this.defaultSigner = new CadesSigner();
	}
	
	SignerPrototype.setAlgorithmName = function(algo){
		if(!!this.signatureAlgorithms[algo]){
			var alg = this.signatureAlgorithms[algo];
			this.algorithmName = alg.Name;  
			return this.sigDigester = this.initDigester(this.hashAlgorithm = alg.HashAlgorithm);
		} else
			return SignerPrototype.prototype.setAlgorithmName.call(this);
			 
	};
	
	/**
	 * @TODO переделать с учетом использования KeyProvider
	 */
	SignerPrototype.createSimpleSignature = function(data){
		if(!this.certificate)
			throw new CertificateError('Signer certificate was not attached');
		this.checkCadesCertificate(true);
		
		if(!this.sigDigester && !this.hashAlgoritm)
			throw new Error("Incorrect HashAlgorithm in SignatureMethod descriptor");
		
		/*if(!this.sigDigester || !CadesDigester || !(this.sigDigester instanceof CadesDigester))
			this.sigDigester = this.initDigester(this.hashAlgorithm); */
		
		this.sigDigester.hashValue(data);
		
		return this.rawSignature.SignHash(this.sigDigester.hashData, this.certificate.certificate);
		
	}
	
	SignerPrototype.createSimpleSignatureHex = function(data){
		var sgn = this.createSimpleSignature(data);
		var hsgn = "";
		for(var i=0;i<data.length;i++){
			hsgn = data.slice(i++,i) + hsgn;
		}
		return hsgn;
	};
	
	
	SignerPrototype.createSimpleSignatureB64 = function(data){
		return Tools.base64_encode(Tools.hexToBinaryString(this.createSimpleSignature(data),true));
	};
	
	SignerPrototype.createSignature = function(data,hx){
		if(this.algorithmName == "http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411"){
			if(hx)
				return this.createSimpleSignatureHex(data);
			return Tools.stringSlice(this.createSimpleSignatureB64(data));
		}
		return SignerPrototype.prototype.createSignature.call(this,data);
	};
	
	
	var dsDigester = XmlDSig.prototype.getDigester();
	
	var builtIn_DigestAlgorithms = {
			"http://www.w3.org/2000/09/xmldsig#sha1": {Name:"http://www.w3.org/2000/09/xmldsig#sha1", Algorithm:0},
			"http://www.w3.org/2000/09/xmldsig#md5": {Name:"http://www.w3.org/2000/09/xmldsig#md5", Algorithm:3},
			"http://www.w3.org/2001/04/xmldsig-more#gostr3411": {Name:"http://www.w3.org/2001/04/xmldsig-more#gostr3411", Algorithm:100}
	};
	
	dsDigester.digestAlgorithms = Object.create(dsDigester.digestAlgorithms);
	
	for(digestAlg in builtIn_DigestAlgorithms)
		dsDigester.digestAlgorithms[digestAlg] 
			= builtIn_DigestAlgorithms[digestAlg];	
	
	var digesterApplyAlgoritmOptions = dsDigester.applyAlgorithmOptions;
	dsDigester.applyAlgorithmOptions = function (options){
		digesterApplyAlgoritmOptions.call(this,options);
		this.hashData = cadesplugin.CreateObject('CAdESCOM.HashedData');;
		this.hashData.Algorithm = this.Algorithm;
		this.hashData.DataEncoding = 1;
	};

	dsDigester.hashValue = function(data){
		this.hashData.Hash(Tools.base64_encode(data));
	};

	dsDigester.getDigestValue = function(data) {
		this.hashData.Hash(Tools.base64_encode(data));
		return this.getHashValueB64();
	}

	dsDigester.getHashValueHex = function(){
		return this.hashData.Value;
	};
	
	dsDigester.getHashValueB64 = function(){
		return Tools.base64_encode(Tools.hexToBinaryString(this.hashData.Value)); 
	};
	
	dsDigester.constructor.prototype = dsDigester;
	
	/*
	 * @TODO удалить
	 *
/*	SignerPrototype.initDigester = function initDigester(algo){
		
		if(!CadesDigester){
		
			var digester = SignerPrototype.prototype.initDigester.call(this); 
			
			var digestAlg;
			digester.digestAlgorithms = Object.create(digester.digestAlgorithms);
			for(digestAlg in builtIn_DigestAlgorithms)
				digester.digestAlgorithms[digestAlg] 
					= builtIn_DigestAlgorithms[digestAlg];
			
			CadesDigester = function(alg) { 
				
				digester.constructor.call(this,alg); 
			};
			CadesDigester.registerAlgorithm = digester.constructor.registerAlgoritm;
			CadesDigester.prototype = digester;
			CadesDigester.prototype.construstor = CadesDigester;
		}
		
		if(!!algo){
			var dig = CadesDigester.prototype.digestAlgorithms[algo].Digester;
			if(dig instanceof CadesDigester)
				return dig;
			return CadesDigester.prototype.digestAlgorithms[algo].Digester = new CadesDigester(algo);
		}
		
		return new CadesDigester();
		
	}; */

	
/*	CadesSigner.prototype.signDigestHex = function(digester){
		if(!!this.certificate)
			var oCert = this.certificate.certificate;
			var oHash = digester.hashData;
			var sSignature = this.rawSignature.SignHash(oHash,oCert);
			return sSignature;
	};
	
	
	CadesSigner.prototype.signDigest = function(digester){
		return Tools.base64_encode(this.signDigestHex(digester));
	}; */
	
	/**
	 * @TDOD Нужно переделать с учетом использования KeyProvider
	 */
	var CertificateError = function (message, filen, linenum){
		this.superClass.call(this,message,filen,linenum);
	}
	
	CertificateError.prototype = new Error();
	CertificateError.prototype.superClass = Error.prototype.constructor; 
	CertificateError.prototype.constructor = CertificateError;
	
	SignerPrototype.getCertificateConstructor = function getCertificateConstructor(){
		return CadesCertificate;
	}
	
	SignerPrototype.checkCadesCertificate = function(sgn){
		
		if(!(this.certificate instanceof CadesCertificate) || !this.certificate.hasCertObj())
			throw new CertificateError("Certificate type error or Certificate object does not properly initialized.");
		
		var dateObj = new Date();
		if(!this.certificate.isValid())
			throw new CertificateError("Certificate validation error")
		
		if(!!sgn){
			
			var dt = new Date();
			if(!(dt<this.certificate.certTillDate))
				throw new CertificateError("Certificate is expired");
			
			if(!this.certificate.isValid())
				throw new CertificateError("Certification chain is invalid");
		}
		
		return true;
	}
	
	
	var CadesKeyProvider = function() {
		
	}
	/**
	 * @todo реализовать п 5.1 - 5.4
	 */
	var CadesX509KeyProvider 
	
	/*var fGetCertificate = SignerPrototype.getCertificate;	
	 
	SignerPrototype.getCertificate = function(){
		if(!!this.certificate)
			return this.certificate;
		return fGetCertificate.call(this);
	}; */
	
	var CadesCertificate = function CadesCertificate(certObj){
		if(!!certObj){
		    this.certificate = certObj;
		    this.certFromDate = new Date(this.certificate.ValidFromDate);
		    this.certTillDate = new Date(this.certificate.ValidToDate);
			this.hasCertObj;
		}
	};
	
	CadesCertificate.prototype = SignerPrototype.getCertificate();
	CadesCertificate.prototype.constructor = CadesCertificate;
	
	CadesCertificate.prototype.applyFromElement = function(){
		
	};
	
	CadesCertificate.useFromLocalStore = function(thumbprint){
		var oStore = cadesplugin.CreateObject("CAPICOM.store");
		oStore.Open();
		thumbprint = thumbprint.split(" ").reverse().join("").replace(/\s/g, "").toUpperCase();
		var oCerts =oStore.Certificates.Find(0,thumbprint);
		if(oCerts.Count==0)
			throw "Certificate thumbprint not found";
		return new CadesCertificate(oCerts.Item(1));
	};
	
	
	CadesCertificate.enumLocalStorage = function(callback){
		var oStore = cadesplugin.CreateObject("CAPICOM.store");
		oStore.Open();
		var certCnt = oStore.Certificates.Count 
		if(certCnt==0)
			throw new Error("Certificates not found in storage");
		result = [];
		for(var ci=1;ci<=certCnt;ci++){
			var oCert = new CadesCertificate(oStore.Certificates.Item(ci));
			if(!!callback)
				result.push(callback(ci,oCert));
			else
				result.push(oCert.thumbprint());
		}
		
		return result;
	};

	CadesCertificate.prototype.check = function check(digit){
	    return (digit<10) ? "0"+digit : digit;
	};

	CadesCertificate.prototype.extract = function(from, what)
	{
	    certName = "";

	    var begin = from.indexOf(what);

	    if(begin>=0)
	    {
	        var end = from.indexOf(', ', begin);
	        certName = (end<0) ? from.substr(begin) : from.substr(begin, end - begin);
	    }

	    return certName;
	};

	CadesCertificate.prototype.dateTimePutTogether = function(certDate)
	{
	    return this.check(certDate.getUTCDate())+"."+this.check(certDate.getMonth()+1)+"."+certDate.getFullYear() + " " +
	                 this.check(certDate.getUTCHours()) + ":" + this.check(certDate.getUTCMinutes()) + ":" + this.check(certDate.getUTCSeconds());
	};

	CadesCertificate.prototype.getCertString = function()
	{
	    return this.extract(this.certificate.SubjectName,'CN=') 
	    	+ "; Выдан: " + this.getCertFromDate() 
	    	+ "; До: " + this.getCertTillDate();
	};

	CadesCertificate.prototype.getCertFromDate = function()
	{
	    return this.dateTimePutTogether(this.certFromDate);
	};

	CadesCertificate.prototype.getCertTillDate = function()
	{
	    return this.dateTimePutTogether(this.certTillDate);
	};

	CadesCertificate.prototype.getPubKeyAlgorithm = function()
	{
	    return this.cert.PublicKey().Algorithm.FriendlyName;
	};

	CadesCertificate.prototype.getCertName = function()
	{
	    return this.extract(this.certificate.SubjectName, 'CN=');
	};
	
	CadesCertificate.prototype.thumbprint = function()
	{
		return this.certificate.Thumbprint;
	}

	CadesCertificate.prototype.getIssuer = function()
	{
	    return this.extract(this.certificate.IssuerName, 'CN=');
	};
	
	CadesCertificate.prototype.import = function(){
		
	};
	
	CadesCertificate.prototype.hasCertObj = function(){
		return !!this.certificate;
	}
	
	CadesCertificate.prototype.isValid = function(){
		return this.hasCertObj()?!!this.certificate.IsValid().Result:false;
	}
	
	var builtIn_SignatureMethods = {
				"http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411":
				{Name:"http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411", 
				HashAlgorithm: "http://www.w3.org/2001/04/xmldsig-more#gostr3411"}
	}
	

}());