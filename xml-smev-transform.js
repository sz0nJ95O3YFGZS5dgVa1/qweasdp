/**
 * Реализация трансформации СМЭВ для XMLDsig
 */
;(function(){
								 
	var c14n = XmlDSig.prototype.getTransform("http://www.w3.org/2001/10/xml-exc-c14n#");
	
	
	var Smev  = function(options){
		if(!(this instanceof Smev)){
			return new smev(options);
		}
		c14n.call(this,options);
	};
	
	Smev.prototype = c14n;
	c14n = c14n.constructor;
	Smev.prototype.constructor = Smev;
	
	/**
	 * Трансформация СМЭВ "поглощает" трансформацию c14n
	 * @param otherTransforms
	 * @returns {Array}
	 */
	Smev.prototype.prepareToDo = function(otherTransforms){
		if(!(otherTransforms instanceof Array))
			otherTransforms = [];
		var newTransforms = [];
		for(var i=0;i<otherTransforms.length;i++ ){
			if(!(otherTransforms[i] instanceof this.constructor)
					// Подразумевает поглощение
					&& !(otherTransforms[i] instanceof c14n))
				newTransforms.push(otherTransforms[i]);
		}
		newTransforms.push(this);
		return newTransforms;
	}
	
	/*
	 *  Пункт 7.1 ПРИЛОЖЕНИЕ 1 Методические рекомендации по работе СМЭВ версия 3.0.9.8.1
	 */
	
	/**
	 * Пунк 1. XML declaration и processing instructions, если есть, вырезаются
	 */ 
	Smev.prototype._renderProcessingInstruction = function(node){
		return "";
	};
	
	
	/**
	 * Пункт 2 Если текстовый узел содержит только пробельные символы (код символа меньше или равен '\u0020'), этот текстовый узел вырезается.
	 */
	Smev.prototype._renderText = function(node){
		var text = node.data.replace(/^[\s]+$/,"");
		if(text!="")
			text = c14n.prototype._renderText.call(this,node);
		return text;
	};
	
	/*
	 * Пункт 3, 4, 5 Реализованы в рамках родителя: Каноникализации c14n-exc
	 */
	
	/**
	 * Пункты 6, 8 
	 */
	Smev.prototype._renderNamespace  = function _renderNamespace(
			node, prefixesInScope, defaultNamespace) {
		var res = "", newDefaultNamespace = defaultNamespace, newPrefixesInScope = prefixesInScope
				.slice(), nsListToRender = [];

		var currentNamespace = node.namespaceURI || "";
		var newPrefix = "";
		
		prefixesInScope.ns = prefixesInScope.ns || {num: 1};
		newPrefixesInScope.ns = prefixesInScope.ns;
		
		if(node.namespaceURI){
				
			var foundPrefix = newPrefixesInScope.filter(function(e) {
					return e.namespaceURI === currentNamespace}).shift();
		
			if (!foundPrefix) {
				newPrefixesInScope.push( 
						foundPrefix = {
							prefix : node.prefix,
							namespaceURI : node.namespaceURI,
							order: prefixesInScope.ns.num++, 
						});
				
				nsListToRender.push({
					prefix : 'ns'+foundPrefix.order,
					namespaceURI : node.namespaceURI,
					order: 0,
				});
			}
			
			newPrefix = 'ns'+foundPrefix.order;
		}
		
		var i =1;
		
		if (node.attributes) {
			var prefixToAdd = [];
			
			for (var i = 0; i < node.attributes.length; i++) {
				
				var attr = node.attributes[i], foundPrefix = null;

				if (attr.prefix && attr.prefix !== "xmlns" && attr.prefix !=='xml') {
						foundPrefix = newPrefixesInScope.filter(function(e) {
							return e.namespaceURI === attr.namespaceURI;
						}).shift();
					}

				if (attr.prefix && !foundPrefix && attr.prefix !== "xmlns" && attr.prefix !== 'xml') {
					
					prefixToAdd.push(
							{
								prefix: attr.prefix,
								namespaceURI: attr.namespaceURI,
							}
					)
					
				} else if (attr.prefix
						&& attr.prefix === "xmlns"
						&& this.inclusiveNamespaces.indexOf(attr.localName) !== -1) 
				{
					prefixToAdd.push(
							{
								prefix: attr.localName,
								namespaceURI: attr.nodeValue,
							}
					);
	
				}
			}
			
			prefixToAdd.sort(function(a,b){
					return a.namespaceURI.localeCompare(b.namespaceURI)
					} ).map(function(attr){
						
							newPrefixesInScope.push( 
								foundPrefix = {
									prefix : attr.prefix,
									namespaceURI : attr.namespaceURI,
									order: prefixesInScope.ns.num++,
								});
							
							nsListToRender.push({
								prefix : 'ns'+foundPrefix.order,
								namespaceURI : attr.namespaceURI,
								order: i++,
							});
					});			
		}

		for (var i = 0; i < nsListToRender.length; ++i) {
			res += " xmlns:"
					+ nsListToRender[i].prefix
					+ "=\""
					+ this.escape
							.attributeEntities(nsListToRender[i].namespaceURI)
					+ "\"";
		}
		
		return {
			rendered : res,
			newPrefix: newPrefix, 
			newDefaultNamespace : currentNamespace,
			newPrefixesInScope : newPrefixesInScope,
		};
	};
	
	
	/**
	 * Пункт 7
	 */
	Smev.prototype._renderAttributes = function _renderAttributes(
			node, newPrefixesInScope) {
		var escape = this.escape;
		return (node.attributes ? [].slice.call(node.attributes) : [])
				.filter(function(attribute) {
					return attribute.name.indexOf("xmlns") !== 0;
				})
				.sort(
					function(a,b){
						if(!a.namespaceURI && b.namespaceURI)
							return 1;
						if(!b.namespaceURI && a.namespaceURI)
							return -1;
						var comp = (b.namespaceURI && a.namespaceURI)?
									a.namespaceURI.localeCompare(b.namespaceURI):0;
						if(comp===0)
							return a.localName.localeCompare(b.localName);
						return comp;
					}).map(
						function(attribute) {
							var prefix = null;
							return " "
									+ ( (prefix = newPrefixesInScope.filter(function(e){
										  			return e.namespaceURI == attribute.namespaceURI 
												}).shift()
										)?'ns'+prefix.order+":":"")
									+ attribute.localName
									+ "=\""
									+ escape.attributeEntities(attribute.value)
									+ "\"";
						}).join("");
	};

	
	
	XmlDSig.prototype.addTransformConstructor("urn://smev-gov-ru/xmldsig/transform",Smev)
	
})();