
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}


function plugin_check(){
	
    var isPluginLoaded = false;
    var isPluginEnabled = false;
    var isPluginWorked = false;
    var isActualVersion = false;
    
	try {
		
	  var oAbout = cadesplugin.CreateObject("CAdESCOM.About");
      var CurrentPluginVersion = oAbout.PluginVersion;
      if( typeof(CurrentPluginVersion) == "undefined")
          CurrentPluginVersion = oAbout.Version;
      
      var cspVersion = oAbout.CSPVersion("", 75);
      
      isPluginLoaded = true;
      isPluginEnabled = true;
      isPluginWorked = true;
      
      document.getElementById('PluginEnabledImg').setAttribute("src", "Img/green_dot.png");
      document.getElementById('PlugInEnabledTxt').innerHTML = "Плагин загружен.";
      document.getElementById('PlugInVersionTxt').innerHTML = "Версия плагина: " + CurrentPluginVersion.MajorVersion + "." + CurrentPluginVersion.MinorVersion + "." + CurrentPluginVersion.BuildVersion;
      document.getElementById('CSPVersionTxt').innerHTML = " Версия CSP: "+cspVersion.MajorVersion + "." + cspVersion.MinorVersion + "." + cspVersion.BuildVersion;
      
	} catch(err){
        var mimetype = navigator.mimeTypes["application/x-cades"];
        if (mimetype) {
            isPluginLoaded = true;
            var plugin = mimetype.enabledPlugin;
            if (plugin) {
                isPluginEnabled = true;
            }
        }
	}
}


function certs4choice(elid){
	var signer = XmlDSig.prototype.getDefaultSigner.call({}); 	
	var Certificate = signer.getCertificateConstructor();
	var certList = document.getElementById(elid);
	Certificate.enumLocalStorage( 
			function(ci,oCert){
				var option = document.createElement('option');
					option = certList.appendChild(option);
					option.setAttribute('value',oCert.thumbprint());
					option.textContent = oCert.getCertString();
				return option;
	});

}